# translation of uk_UA.po to Ukrainian
# uk translation of gtklp.
# Copyright (C) 2002 Tobias Miller
# This file is distributed under the same license as the PACKAGE package.
# Mikhailo Lytvyn <misha@mao.kiev.ua> 2006.
# 
# Mikhailo Lytvyn <misha@mao.kiev.ua>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: uk_UA\n"
"Report-Msgid-Bugs-To: gtklp@sirtobi.com\n"
"POT-Creation-Date: 2007-09-08 14:32+0200\n"
"PO-Revision-Date: 2007-09-07 16:31+0300\n"
"Last-Translator: Mikhailo Lytvyn <misha@mao.kiev.ua>\n"
"Language-Team: Ukrainian\n"
"Language: uk_UA\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: gtklp/file.c:285 gtklp/file.c:288
msgid "Print File..."
msgstr "Виберіть файл для друку"

#: gtklp/file.c:435
msgid "Files to print"
msgstr "Перелік файлів"

#: gtklp/file.c:473
msgid "Add"
msgstr "Додати "

#: gtklp/file.c:482 gtklp/gtklp_functions.c:2963
msgid "Remove"
msgstr "Видалити "

#: gtklp/file.c:491
msgid "Remove All"
msgstr "Видалити всі"

#: gtklp/file.c:516
msgid "File"
msgstr "Файл"

#. PrinterLocation
#: gtklp/general.c:74
msgid "Location"
msgstr "Розміщення"

#: gtklp/general.c:455
msgid "Duplex Settings"
msgstr "Двохсотонній друк"

#: gtklp/general.c:479
msgid "No Duplex"
msgstr "Односторонній друк"

#: gtklp/general.c:496
msgid "Long edge binding"
msgstr "книжнкова оправа"

#: gtklp/general.c:513
msgid "Short edge binding"
msgstr "альбомна оправа"

#: gtklp/general.c:542
msgid "Media Selections"
msgstr "Папір"

#: gtklp/general.c:556
msgid "Media Size"
msgstr "розмір"

#: gtklp/general.c:631
msgid "Media Type"
msgstr "тип"

#: gtklp/general.c:653
msgid "Media Source"
msgstr "джерело"

#: gtklp/general.c:704
msgid "Number of Copies"
msgstr "Кількість копій"

#: gtklp/general.c:724
msgid "Collate Copies"
msgstr "співставляти копії"

#: gtklp/general.c:924
msgid "Other actions"
msgstr "Інші дії"

#. Button
#: gtklp/general.c:935
msgid "Call GtkLPQ"
msgstr "Показати чергу друку"

#: gtklp/general.c:955 gtklpq/printer.c:161
msgid "Printer"
msgstr "Принтер"

#. Add Favorites Button
#: gtklp/general.c:989
msgid "Add favorite"
msgstr ""

#. Remove Favorites Button
#: gtklp/general.c:996
msgid "Remove favorite"
msgstr ""

#: gtklp/general.c:1041
msgid "General"
msgstr "Основні"

#: gtklp/gtklp.c:266
msgid "Please wait!"
msgstr "Зачекайте..."

#: gtklp/gtklp.c:290 gtklpq/gtklpq.c:306
msgid "This option requires an value !"
msgstr "Необхідно вказати значення параметра."

#: gtklp/gtklp.c:327
msgid ""
"Usage: gtklp [-P|-d printer] [-c configdir] [-S server] [-U user] [-p port] "
"[-l] [-D] [-V] [-b] [-i] [-# n] [-C] [-H] [-E] [-J jobname] [-q priority] [-"
"o option=value ...] [file(s)]\n"
msgstr ""
"Допустимі параметри: gtklp [-P|-d принтер] [-c директорія збереження "
"налаштувань] [-S сервер] [-U користувач] [-p порт] [-l] [-D] [-V] [-b] [-"
"i]·[-#·n] [-C] [-H] [-E] [-J им'я завдання] [-q пріоритет] [-o "
"параметр=значення ...] [файл(и)]\n"

#: gtklp/gtklp.c:340
msgid "Function is not supported for now !"
msgstr "На жаль, gtklp поки що не підтримує цю функцію CUPS"

#: gtklp/gtklp.c:369 gtklpq/gtklpq.c:261
msgid "Invalid Portnumber !"
msgstr "Хибний номер порта!"

#: gtklp/gtklp.c:397 gtklpq/gtklpq.c:281
msgid "Invalid Printername !"
msgstr "Хибне ім'я принтера!"

#: gtklp/gtklp.c:416 gtklpq/gtklpq.c:317
msgid "Invalid Servername !"
msgstr "Хибне ім'я сервера!"

#: gtklp/gtklp.c:428 gtklpq/gtklpq.c:329
msgid "Invalid Username !"
msgstr "Хибне ім'я користувача!"

#: gtklp/gtklp.c:469 gtklp/gtklptab.c:678 gtklpq/gtklpq.c:338
msgid "version"
msgstr "версія "

#: gtklp/gtklp.c:536
#, c-format
msgid "-E: %s\n"
msgstr "-E: %s\n"

#: gtklp/gtklp.c:538 gtklpq/gtklpq.c:364
msgid "Sorry, this function is not compiled in !"
msgstr "На жаль, ця функція не була включена при компіляції."

#: gtklp/gtklp.c:572
msgid "Too many files to print !"
msgstr " Забагато файлів у черзі друку!"

#: gtklp/gtklp.c:624 gtklpq/gtklpq.c:387
msgid "No Cups-Server found !"
msgstr "Не знайдений сервер CUPS!"

#: gtklp/gtklp.c:652 gtklpq/gtklpq.c:399
#, c-format
msgid "Unable to connect to Server %s !"
msgstr "Не можу з'єднатися з сервером %s!"

#: gtklp/gtklp.c:708 gtklpq/gtklpq.c:450
msgid "No Printer found to use as default !"
msgstr "Немає принтера, який можна використовувати за замовчуванням! "

#: gtklp/gtklp.c:737
msgid "Print"
msgstr " Друк "

#: gtklp/gtklp.c:751
msgid "Reset All"
msgstr " Скидання налаштувань "

#: gtklp/gtklp.c:763
msgid "Templates"
msgstr " Збереження налаштувань "

#: gtklp/gtklp.c:775
msgid "Close"
msgstr "Закрити"

#: gtklp/gtklp_functions.c:916
msgid ""
"The PPD for this printer has changed!\n"
"Resetting..."
msgstr ""
"Змінений драйвер принтера!\n"
"Зачекайте..."

#: gtklp/gtklp_functions.c:1560 gtklp/gtklp_functions.c:1584
#: gtklp/gtklp_functions.c:1699
msgid "Error saving user-settings!"
msgstr "Помилка збереження налаштувань користувача!   "

#: gtklp/gtklp_functions.c:1643
msgid "Too many printers!"
msgstr "Забагато принтерів!   "

#: gtklp/gtklp_functions.c:1654
msgid "Unable to add instance!"
msgstr "Помилка збереження варіанту налаштувань!"

#: gtklp/gtklp_functions.c:2169 gtklp/gtklp_functions.c:2174
#: gtklp/gtklp_functions.c:2781
msgid "Options saved!"
msgstr "Налаштування збережені!"

#: gtklp/gtklp_functions.c:2244
msgid "Instance removed!"
msgstr "Варіант видалено!"

#: gtklp/gtklp_functions.c:2253
msgid "Saved Defaults loaded!"
msgstr "Відновлено стандартні налаштування друку!"

#: gtklp/gtklp_functions.c:2324
msgid ""
"Unable to print,\n"
"unknown file format!"
msgstr "Невідомий формат файла!"

#: gtklp/gtklp_functions.c:2329 gtklp/gtklp_functions.c:2396
msgid "JobID"
msgstr "Номер завдання"

#: gtklp/gtklp_functions.c:2337
msgid "Dont know what to print!"
msgstr "Не вибрано документ!"

#: gtklp/gtklp_functions.c:2357 gtklp/gtklp_functions.c:2366
#: gtklp/gtklp_functions.c:2392
msgid "Unable to print!"
msgstr "Помилка друку!"

#: gtklp/gtklp_functions.c:2388
msgid "(stdin)"
msgstr "(stdin)"

#: gtklp/gtklp_functions.c:2503 gtklp/gtklp_functions.c:2509
msgid "Conflicts"
msgstr "Конфлікти"

#: gtklp/gtklp_functions.c:2556 gtklp/gtklptab.c:753 libgtklp/libgtklp.c:130
#: libgtklp/libgtklp.c:741 libgtklp/libgtklp.c:958
msgid "Ok"
msgstr " Гаразд "

#: gtklp/gtklp_functions.c:2793 gtklp/gtklp_functions.c:2816
#: gtklp/gtklp_functions.c:2958 gtklp/gtklptab.c:952
msgid "Save"
msgstr "Зберегти"

#: gtklp/gtklp_functions.c:2829
msgid "New"
msgstr "Створити"

#: gtklp/gtklp_functions.c:2863 gtklp/gtklp_functions.c:2869
msgid "Printer Templates"
msgstr "Варіанти налаштувань друку"

#: gtklp/gtklp_functions.c:2900
msgid "Printer: "
msgstr "Принтер"

#: gtklp/gtklp_functions.c:2905
msgid "Instance: "
msgstr "Варіант"

#: gtklp/gtklp_functions.c:2971 libgtklp/libgtklp.c:745
msgid "Cancel"
msgstr "Відмінити"

#: gtklp/gtklptab.c:288
msgid "Look"
msgstr "Показувати вкладки"

#: gtklp/gtklptab.c:305
msgid "Show Output"
msgstr "Друк"

#: gtklp/gtklptab.c:312
msgid "Show Text"
msgstr "Текст"

#: gtklp/gtklptab.c:319
msgid "Show Image"
msgstr "Зображення"

#: gtklp/gtklptab.c:326
msgid "Show HP-GL/2"
msgstr "HP-GL/2"

#: gtklp/gtklptab.c:333
msgid "Show Special"
msgstr "Додатково"

#: gtklp/gtklptab.c:339
msgid "Show PPD"
msgstr "Драйвер"

#: gtklp/gtklptab.c:452
msgid "Feel"
msgstr "Поведінка програми"

#: gtklp/gtklptab.c:470
msgid "Remember last printer"
msgstr "запам'ятовувати останній використаний принтер"

#: gtklp/gtklptab.c:477
msgid "Remember last Tab"
msgstr "запам'ятати відкриту вкладку"

#: gtklp/gtklptab.c:486
msgid "Save all preferences on exit"
msgstr "зберігати налаштування при виході"

#: gtklp/gtklptab.c:495
msgid "Save number of copies on exit"
msgstr "запам'ятовувати число копій при виході"

#: gtklp/gtklptab.c:503
msgid "Clear file list on print"
msgstr "скидати список файлів після закінчення друку"

#: gtklp/gtklptab.c:510
msgid "Exit on print"
msgstr "закінчувати роботу програми після закінчення друку"

#: gtklp/gtklptab.c:517
msgid "Show JobIDs"
msgstr "показувати номери завдань"

#: gtklp/gtklptab.c:529
msgid "Enable Constraints check"
msgstr "використовувати перевірку обмежень"

#: gtklp/gtklptab.c:535
msgid "Check now"
msgstr "Перевірити"

#: gtklp/gtklptab.c:545
msgid "Remember size and position"
msgstr "запам'ятати розмір і положення"

#. from
#: gtklp/gtklptab.c:690
msgid "written by"
msgstr "Автор:"

#. for
#: gtklp/gtklptab.c:717
msgid "written for cups"
msgstr "Система друку CUPS"

#: gtklp/gtklptab.c:787
msgid "Help"
msgstr "Допомога"

#. Buttons
#: gtklp/gtklptab.c:798
msgid "Help on Options"
msgstr "Опис опцій CUPS  "

#: gtklp/gtklptab.c:805
#, c-format
msgid "About %s"
msgstr "  Про програму %s"

#: gtklp/gtklptab.c:846
msgid "Commands and Paths"
msgstr "Шляхи і команди"

#: gtklp/gtklptab.c:859
msgid "Browser Command"
msgstr "команда запуска програми перегляду HTML"

#: gtklp/gtklptab.c:872
msgid "Help URL"
msgstr "розміщення файла довідки"

#: gtklp/gtklptab.c:885
msgid "Gtklpq Command"
msgstr "команда Gtklpq"

#: gtklp/hpgl2.c:114 gtklp/special.c:353
msgid "Options"
msgstr "Налаштування"

#: gtklp/hpgl2.c:131
msgid "Printing in Black"
msgstr "чорно-білий друк"

#: gtklp/hpgl2.c:138
msgid "Fit on Page"
msgstr "підігнати до розміру сторінки"

#: gtklp/hpgl2.c:185
msgid "Pen Width"
msgstr "Товщина ліній"

#. label
#: gtklp/hpgl2.c:205
msgid "micrometers"
msgstr "микрометрів (1000 микрометрів = 1 міліметр)"

#. Buttons
#. Buttons CPI
#. Buttons LPI
#. Buttons CPI
#: gtklp/hpgl2.c:210 gtklp/image.c:304 gtklp/image.c:358 gtklp/output.c:767
#: gtklp/output.c:846 gtklp/text.c:270 gtklp/text.c:297 gtklp/text.c:324
msgid "Default"
msgstr "Стандарт"

#: gtklp/hpgl2.c:228
msgid "HP-GL/2"
msgstr "HP-GL/2"

#: gtklp/image.c:138 gtklp/image.c:231
msgid "None"
msgstr "відсутнє"

#: gtklp/image.c:140 gtklp/image.c:232
msgid "Sheet relativ"
msgstr "відносно листа"

#: gtklp/image.c:142 gtklp/image.c:233
msgid "Image relativ"
msgstr "відносно зображення"

#: gtklp/image.c:144 gtklp/image.c:234
msgid "PPI"
msgstr "роздільна здатність (точок на дюйм)"

#: gtklp/image.c:212
msgid "Scaling"
msgstr "Масштабування"

#: gtklp/image.c:277
msgid "Color HUE rotation"
msgstr "Тон"

#: gtklp/image.c:310 gtklp/image.c:364 gtklp/output.c:773 gtklp/output.c:852
msgid "Reset"
msgstr "Скинути"

#: gtklp/image.c:331
msgid "Color saturation"
msgstr "Насиченість"

#: gtklp/image.c:433
msgid "Position"
msgstr "Розміщення"

#: gtklp/image.c:491
msgid "Image"
msgstr "Зображення"

#: gtklp/output.c:413
msgid "Sheet Usage"
msgstr "Лист"

#: gtklp/output.c:425
msgid "Sheets per page"
msgstr "сторінок на листі:"

#: gtklp/output.c:456
msgid "Layout"
msgstr "розміщення сторінок"

#: gtklp/output.c:491
msgid "Border"
msgstr "рамка"

#: gtklp/output.c:577
msgid "Orientation"
msgstr "орієнтація"

#: gtklp/output.c:609
msgid "Mirror Output"
msgstr "Відобразити дзеркально"

#: gtklp/output.c:633
msgid "Ranges"
msgstr "Сторінки"

#: gtklp/output.c:645
msgid "Print Range"
msgstr "окремі сторінки або групи сторінок"

#: gtklp/output.c:659
msgid "(e.g. 1-4,9,10-12)"
msgstr "Наприклад: 1-4, 9, 10-12"

#: gtklp/output.c:670
msgid "All"
msgstr "усі"

#: gtklp/output.c:677
msgid "Even"
msgstr "парні"

#: gtklp/output.c:683
msgid "Odd"
msgstr "непарні"

#: gtklp/output.c:689
msgid "Reverse Output Order"
msgstr "в зворотному порядку"

#: gtklp/output.c:719
msgid "Brightness"
msgstr "Яскравість"

#: gtklp/output.c:798
msgid "Gamma correction"
msgstr "Гама"

#: gtklp/output.c:868
msgid "Output"
msgstr "Друк"

#: gtklp/ppd.c:441
msgid "PPD"
msgstr "Драйвер"

#: gtklp/special.c:116
msgid "Banners"
msgstr "Титульна сторінка"

#. Start
#: gtklp/special.c:139
msgid "Start:"
msgstr "до  "

#. Stop
#: gtklp/special.c:162
msgid "End:"
msgstr "після"

#: gtklp/special.c:206
msgid ""
"You have chosen to store your password on the disk.\n"
"This is not secure, so I recommend against doing so.\n"
"You have been warned."
msgstr ""
"Не радимо зберігати пароль на диску,\n"
"оскільки це може бути небезпечно."

#: gtklp/special.c:222
msgid "Password"
msgstr "Пароль"

#. Login
#: gtklp/special.c:245 libgtklp/libgtklp.c:703
msgid "Login:"
msgstr "Ім'я"

#. Password
#: gtklp/special.c:261 libgtklp/libgtklp.c:708
msgid "Password:"
msgstr "Пароль"

#: gtklp/special.c:300
msgid "Job Name"
msgstr "Документ"

#: gtklp/special.c:369
msgid "Raw output"
msgstr ""
"не використовувати драйвери CUPS (програма може напряму керувати принтером)"

#: gtklp/special.c:395
msgid "Extra Options"
msgstr "Команди керування CUPS"

#: gtklp/special.c:428
msgid "Special"
msgstr "Додатково"

#: gtklp/text.c:238
msgid "Sizes and Spacings"
msgstr "Розміри і відступи"

#. Label CPI
#: gtklp/text.c:254
msgid "Chars per Inch"
msgstr "літер на дюйм"

#. Label LPI
#: gtklp/text.c:281
msgid "Lines per Inch"
msgstr "рядків на дюйм"

#. Label CPP
#: gtklp/text.c:308
msgid "Columns per Page"
msgstr "колонок на сторінку"

#: gtklp/text.c:474
msgid "Margins"
msgstr "Поля"

#. Label Top
#: gtklp/text.c:490
msgid "Top"
msgstr "вгорі"

#. Label Bottom
#: gtklp/text.c:541
msgid "Bottom"
msgstr "знизу"

#. Label Left
#: gtklp/text.c:593
msgid "Left"
msgstr "ліворуч"

#. Label Right
#: gtklp/text.c:644
msgid "Right"
msgstr "праворуч"

#: gtklp/text.c:736
msgid "Text formatting"
msgstr "Форматування"

#: gtklp/text.c:753
msgid "Use Pretty Print"
msgstr ""
"використовувати автоформатування (для друку вихідних текстів на C та C++)"

#: gtklp/text.c:757
msgid "Wrap text"
msgstr "перенос рядків"

#: gtklp/text.c:773
msgid "Text"
msgstr "Текст"

#. --- Info ---
#: libgtklp/libgtklp.c:152
msgid "Message"
msgstr "Повідомлення"

#. --- Error ---
#: libgtklp/libgtklp.c:169 libgtklp/libgtklp.c:890 libgtklp/libgtklp.c:903
#: gtklpq/printer.c:691 gtklpq/printer.c:992
msgid "Error"
msgstr "Помилка"

#: libgtklp/libgtklp.c:187
msgid "Fehlerteufel"
msgstr "Помилка!"

#: libgtklp/libgtklp.c:199
msgid "Warning!"
msgstr "Увага!"

#: libgtklp/libgtklp.c:537 libgtklp/libgtklp.c:565
msgid "No printers found !"
msgstr "Принтер не знайдено!"

#: libgtklp/libgtklp.c:550 libgtklp/libgtklp.c:552
msgid "The given printer does not exist!"
msgstr "Вказаний принтер не виявлено!"

#: libgtklp/libgtklp.c:594
msgid "Internal program error !"
msgstr "Внутрішня помилка!"

#: libgtklp/libgtklp.c:661 libgtklp/libgtklp.c:671
msgid "Password verification"
msgstr "Перевірка пароля"

#: gtklpq/gtklpq.c:223
msgid "GtkLPQ"
msgstr "GtkLPQ"

#: gtklpq/gtklpq.c:299
msgid ""
"Usage: gtklpq [-P|-d Printer] [-S server] [-p port] [-D] [-V] [-U user] [-t "
"timeout] [-g geometry] [-C] [-h] [-E]"
msgstr ""
"Використання: gtklpq [-P|-d принтер] [-S сервер] [-p порт] [-D] [-V] [-U "
"користувач] [-t інтервал] [-g позиція] [-C] [-h] [-E]"

#: gtklpq/gtklpq.c:560 gtklpq/gtklpq.c:561
msgid "Cancel Job"
msgstr "Відмінити"

#: gtklpq/gtklpq.c:574 gtklpq/gtklpq.c:575
msgid "Cancel All"
msgstr "Відмінити все"

#: gtklpq/gtklpq.c:588 gtklpq/gtklpq.c:589 gtklpq/printer.c:262
#: gtklpq/printer.c:263 gtklpq/printer.c:951 gtklpq/printer.c:953
msgid "Hold Job"
msgstr "Призупинити"

#: gtklpq/gtklpq.c:602 gtklpq/gtklpq.c:603
msgid "Move Job"
msgstr "Змінити принтер"

#: gtklpq/gtklpq.c:618 gtklpq/gtklpq.c:619 gtklpq/printer.c:521
#: gtklpq/printer.c:523
msgid "Reject Jobs"
msgstr "Відхилити завдання"

#: gtklpq/gtklpq.c:634 gtklpq/gtklpq.c:635 gtklpq/printer.c:483
#: gtklpq/printer.c:485
msgid "Stop Printer"
msgstr "Зупинити принтер"

#: gtklpq/gtklpq.c:648 gtklpq/gtklpq.c:649
msgid "Priority"
msgstr "Пріоритет"

#: gtklpq/gtklpq.c:669
msgid "Exit"
msgstr "Вийти"

#. Hold 4
#: gtklpq/printer.c:243 gtklpq/printer.c:846
msgid "H"
msgstr "П"

#: gtklpq/printer.c:251 gtklpq/printer.c:252 gtklpq/printer.c:932
#: gtklpq/printer.c:934
msgid "Release Job"
msgstr "Продовжити"

#. Active 5
#: gtklpq/printer.c:266 gtklpq/printer.c:842
msgid "A"
msgstr "А"

#: gtklpq/printer.c:280 gtklpq/printer.c:335 gtklpq/printer.c:358
#: gtklpq/printer.c:714 gtklpq/printer.c:738
msgid "Rank"
msgstr "Місце"

#: gtklpq/printer.c:281 gtklpq/printer.c:340 gtklpq/printer.c:364
#: gtklpq/printer.c:715 gtklpq/printer.c:740
msgid "Owner"
msgstr "Власник"

#: gtklpq/printer.c:282 gtklpq/printer.c:345 gtklpq/printer.c:370
#: gtklpq/printer.c:716 gtklpq/printer.c:742
msgid "Job"
msgstr "Завдання"

#: gtklpq/printer.c:283 gtklpq/printer.c:351 gtklpq/printer.c:377
msgid "File(s)"
msgstr "Документ(и)"

#: gtklpq/printer.c:284 gtklpq/printer.c:717 gtklpq/printer.c:746
msgid "#"
msgstr "#"

#: gtklpq/printer.c:285 gtklpq/printer.c:718 gtklpq/printer.c:748
msgid "Size"
msgstr "Размір"

#: gtklpq/printer.c:298
msgid "Queue"
msgstr "Черга"

#: gtklpq/printer.c:415
msgid "Printer Idle"
msgstr "Принтер вільний"

#: gtklpq/printer.c:420
msgid "Printer Processing"
msgstr "Принтер зайнятий"

#: gtklpq/printer.c:425
msgid "Printer Stopped"
msgstr "Принтер зупинений"

#: gtklpq/printer.c:465 gtklpq/printer.c:467
msgid "Start Printer"
msgstr "Запустити принтер"

#: gtklpq/printer.c:503 gtklpq/printer.c:505
msgid "Accept Jobs"
msgstr "Приймати завдання"

#: gtklpq/printer.c:719 gtklpq/printer.c:744
msgid "Job(s)"
msgstr "Завдання"

#: gtklpq/printer.c:779
msgid "Untitled"
msgstr "без назви"

#. Pending 3
#: gtklpq/printer.c:850
msgid "P"
msgstr "О"

#: gtklpq/printer.c:854
msgid "S"
msgstr "С"

#: gtklpq/printer.c:858
msgid "ca"
msgstr "відм"

#: gtklpq/printer.c:862
msgid "ab"
msgstr "зуп"

#: gtklpq/printer.c:866
msgid "."
msgstr "."

#: gtklpq/printer.c:1002
msgid "No jobs."
msgstr "Немає завдань."

#: gtklpq/printer.c:1036 gtklpq/printer.c:1040 gtklpq/printer.c:1049
msgid "Are you sure?"
msgstr "GtkLPQ"

#: gtklpq/printer.c:1110
msgid "And for what reason ?"
msgstr "Вкажіть причину:"

#: gtklpq/printer.c:1145
msgid "Yes"
msgstr "Так"

#: gtklpq/printer.c:1149
msgid "No"
msgstr "Ні"

#: gtklpq/printer.c:1163
msgid "Do you really want to cancel this job?"
msgstr "Відмінити завдання?"

#: gtklpq/printer.c:1174
msgid "Do you really want to cancel ALL jobs?"
msgstr "Відмінити всі завдання?"

#: gtklpq/printer.c:1186
msgid "Do you really want to hold this job?"
msgstr "Призупинити друк завдання?"

#: gtklpq/printer.c:1196
msgid "Do you really want to release this job?"
msgstr "Продовжити друк завдання?"

#: gtklpq/printer.c:1211
msgid "Do you really want to move this Job to the following queue?"
msgstr "Виберіть потрібний принтер із списку."

#: gtklpq/printer.c:1223
msgid "Do you really want to stop this printer?"
msgstr "Зупинити принтер?"

#: gtklpq/printer.c:1233
msgid "Do you really want to start this printer?"
msgstr "Запустити принтер?"

#: gtklpq/printer.c:1246
msgid "Do you really want to change the job priority?"
msgstr "Змінити пріоритет завдання?"

#: gtklpq/printer.c:1259
msgid "Do you really want to reject jobs for this printer?"
msgstr "Відхилити завдання для цьго принтера?"

#: gtklpq/printer.c:1269
msgid "Do you really want to accept jobs for this printer?"
msgstr "Приймати завдання для цього принтера?"

#: gtklpq/printer.c:1348 gtklpq/printer.c:1358
msgid "Unable to change priority!"
msgstr "Неможливо змінити пріоритет!"

#: gtklpq/printer.c:1542
msgid "Job or printer not found!"
msgstr "Принтер чи завдання не виявлено!"

#: gtklpq/printer.c:1546
msgid "Authorization failed!"
msgstr "Помилка авторизації!"

#: gtklpq/printer.c:1549
msgid "You don't own this job!"
msgstr "Ви не власник цього завдання!"

#: gtklpq/printer.c:1553 gtklpq/printer.c:1589
msgid "Unable to do so!"
msgstr "Неможливо виконати!"

#: gtklpq/printer.c:1561
msgid "Unable to cancel job(s)!"
msgstr "Неможливо відмінити завдання!"

#: gtklpq/printer.c:1566
msgid "Unable to cancel printer!"
msgstr "Неможливо зупинити принтер!"

#: gtklpq/printer.c:1572
msgid "Unable to start printer!"
msgstr "Неможливо запустити принтер!"

#: gtklpq/printer.c:1578
msgid "Unable to accept jobs for this printer!"
msgstr "Неможливо приймати завдання для цього принтера!"

#: gtklpq/printer.c:1584
msgid "Unable to reject jobs for this printer!"
msgstr "Неможливо відхиляти завдання для цього принтера!"

#~ msgid "You have installed too many printers or templates !"
#~ msgstr "Забагато принтерів чи збережених вариантів налаштувань!"
